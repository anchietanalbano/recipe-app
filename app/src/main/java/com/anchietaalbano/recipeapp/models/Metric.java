package com.anchietaalbano.recipeapp.models;

public class Metric{
    public double amount;
    public String unitShort;
    public String unitLong;
}
