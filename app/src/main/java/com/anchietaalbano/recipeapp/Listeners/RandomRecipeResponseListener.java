package com.anchietaalbano.recipeapp.Listeners;

import com.anchietaalbano.recipeapp.models.RandomRecipeApiResponse;

public interface RandomRecipeResponseListener {
    void diFetch(RandomRecipeApiResponse response, String message);
    void didError(String message);
}
